import React from 'react';
import PropTypes from 'prop-types';
import Big from 'big.js';
import SignIn from './SignIn';

export default function Form({ onSubmit, currentUser, onSignChange, signNum, hadMsg }) {
  return (
    <form onSubmit={onSubmit}>
      <fieldset id="fieldset">
        <p>Dear, { currentUser.accountId }. Click button to mint NFT to your wallet !</p>

        {hadMsg ? 
          <p>Sory, sign blocked, you already had message.</p>
          :
          <button className="mint-button" type="submit">
            Mint NFT witn NEARⓃ!
          </button>                
        } 
      </fieldset>
    </form>
  );
}

Form.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  currentUser: PropTypes.shape({
    accountId: PropTypes.string.isRequired,
    balance: PropTypes.string.isRequired
  })
};
