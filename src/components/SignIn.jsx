import React from 'react';

export default function SignIn() {
  return (
    <>
      <h1>NFT MINTING with NEAR</h1>
      <p>
          To mint NFT, please login with your NEAR wallet
      </p>
    </>
  );
}
