import 'regenerator-runtime/runtime';
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Big from 'big.js';
import Form from './components/Form';
import SignIn from './components/SignIn';

//const BOATLOAD_OF_GAS = Big(3).times(10 ** 13).toFixed();
const DONATION = Big('0.1').times(10 ** 24).toFixed();
const BOATLOAD_OF_GAS = '100000000000000';

//const DONATION = '10000000000000000000000';

const IMAGE_LINK = 'https://bafybeicpwikgihx6dx36ivkjfsfoouylcxesficdlb4ghirh4vvtmqgzwu.ipfs.dweb.link';
const App = ({ contract, currentUser, nearConfig, wallet }) => {

  const onSubmit = (e) => {
    e.preventDefault();

    const { fieldset } = e.target.elements;

    fieldset.disabled = true;
    // TODO: optimistically update page with new message,
    // update blockchain data in background
    // add uuid to each message, so we know which one is already known
    contract.nft_mint(
      {
        receiver_id: currentUser.accountId,
        token_id: `${Date.now()}${currentUser.accountId}${Math.floor(Math.random() * 10)}`,
        metadata: {
          title: 'Cessna from saddykiller!',
          media: IMAGE_LINK,
          copies: 1
        }
      },
      BOATLOAD_OF_GAS,
      DONATION
    ).then(() => {
      fieldset.disabled = false;
    }).catch( e => {
      fieldset.disabled = false;
    }
    );
  };

  const signIn = () => {
    wallet.requestSignIn(
      nearConfig.contractName,
      'NEAR Guest Book'
    );
  };

  const signOut = () => {
    wallet.signOut();
    window.location.replace(window.location.origin + window.location.pathname);
  };

  return (
    <main>
      <header>
        <h1>NEAR NFT Minting</h1>
        { currentUser
          ? <button onClick={signOut}>Log out</button>
          : <button onClick={signIn}>Log in</button>
        }
        <p>         
          <img className="nft" src={IMAGE_LINK} />
        </p>
       
      </header>
      { currentUser
        ? <Form onSubmit={onSubmit} currentUser={currentUser}/>
        : <SignIn/>
      }
      

    </main>
  );
};

App.propTypes = {
  contract: PropTypes.shape({
    nft_mint: PropTypes.func.isRequired
  }).isRequired,
  currentUser: PropTypes.shape({
    accountId: PropTypes.string.isRequired,
    balance: PropTypes.string.isRequired
  }),
  nearConfig: PropTypes.shape({
    contractName: PropTypes.string.isRequired
  }).isRequired,
  wallet: PropTypes.shape({
    requestSignIn: PropTypes.func.isRequired,
    signOut: PropTypes.func.isRequired
  }).isRequired
};

export default App;
